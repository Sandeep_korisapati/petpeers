<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Registration Page</title>
<link href='<c:url value="/resources/css/registerpage1.css" />'
	rel="stylesheet">
<style type="text/css">

body {
	background-image:linear-gradient(rgba(4, 9, 30, 0.7), rgba(4, 9, 30, 0.7)),
		url("${pageContext.request.contextPath}/resources/images/background.jpg");
	width: 100%;
	background-size: cover;
}
.error{
	color: red;
}
</style>

</head>
<body>
	<div class="titlepage">
	<div class="pagename" ><h1>PET SHOP</h1></div>
	<div class="login"><a href="login">Login</a></div>
	</div>
	<div class="header">
		<div class="register">
			<h1>Register For Account</h1>
			<form:form action="saveUser" method="post"
				modelAttribute="user" class="table">

				<label>Username</label>
				<br>
				<form:input class="inputs" type="text" path="userName" />
				<br>
				<form:errors path="userName" cssClass="error"></form:errors>
				<br>
				<label>Password</label>
				<br>
				<form:input class="inputs" type="password" path="userPassword" />
				<br>
				<form:errors path="userPassword" cssClass="error"></form:errors>
				<br>
				<label>Confirm Password</label>
				<br>
				<form:input class="inputs"  path="confirmPassword" />
				<br>
				<form:errors path="confirmPassword" cssClass="error"></form:errors>
				<br>
				<input type="submit" value="Register" class="button">

			</form:form>
		</div>
	</div>
</body>
</html>