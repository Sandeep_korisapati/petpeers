<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>AddPet</title>
<link href='<c:url value="/resources/css/addPetPage.css" />'
	rel="stylesheet">
<style type="text/css">
body {
  background-image:
		url("${pageContext.request.contextPath}/resources/images/homebg1.jpg");
  background-repeat: no-repeat;
  background-attachment: fixed; 
  background-size: 100% 100%;
}
table {
    border-spacing: 0 10px;
}
.pettable {
	margin-top : 5%;
    border-collapse: collapse;
    border-spacing: 0;
    border-color: #ccc;
}

 

.pettable td {
    font-family: Arial, sans-serif;
    font-size: 16px;
    padding: 16px 8px;
    overflow: hidden;
    word-break: normal;
    border- color: #ccc;
    color: #00FF00
}

 

.pettable th {
    font-family: Arial, sans-serif;
    font-size: 16px;
    font-weight: normal;
    padding: 16px 8px;
    border-style: solid;
    border-width: 2px;
    overflow: hidden;
    word-break: normal;
    border- color: #ccc;
    color: #000000;
}
.error{
	color: red;
}
</style>
</head>
<body>
<div class="topnav">
  <a class="active" href="#"><i class="fa fa-fw fa-home"></i> PETSHOP</a> 

   <div class="topnav-right">
    <a href="login" class="active">Logout</a>
  </div>
  <div class="topnav-right">
  <a href="home"><i class="fa fa-fw fa-search"></i> Home</a> 
  <a href="myPets"><i class="fa fa-fw fa-envelope"></i> My PET</a> 
  <a href="addPetPage"><i class="fa fa-fw fa-user"></i> Add Pet</a>
</div>
</div>
<div class="header">
		<div class="register">
			<h1>ADD PET</h1>
			<form:form name="form"  method="post" modelAttribute="pet"
				class="table">

				<label>Pet Name</label>
				<br>
				<form:input class="inputs" type="text" path="petName" />
				<br>
				<form:errors path="petName" cssClass="error"></form:errors>
				<br>
				<label>Pet Age</label>
				<br>
				<form:input class="inputs" type="text" path="petAge" />
				<br>
				<form:errors path="petAge" cssClass="error"></form:errors>
				<br>
				<label>Pet Place</label>
				<br>
				<form:input class="inputs"  path="petPlace" />
				<br>
				<form:errors path="petPlace" cssClass="error"></form:errors>
				<br>
				<input type="submit" value="Add Pet" class="button" onclick="javascript: form.action='savePet';" >
				<input type="submit" value="Cancel" class="button" onclick="javascript: form.action='addPetPage';">
			</form:form>
		</div>
	</div>
	<!-- <script type="text/javascript">
	function button2() {
		document.form.action = "addPetPage";
		return true;
	}
	
	
	</script> -->
</body>
</html>