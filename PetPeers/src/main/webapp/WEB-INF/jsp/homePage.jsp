<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Home Page</title>
<link href='<c:url value="/resources/css/homepage.css" />'
	rel="stylesheet">
<style type="text/css">
body {
	background-image:
		url("${pageContext.request.contextPath}/resources/images/homebg1.jpg");
	background-repeat: no-repeat;
	background-attachment: fixed;
	background-size: 100% 100%;
}

table {
	border-spacing: 0 10px;
}

.pettable {
	border-collapse: collapse;
	border-spacing: 0;
	border-color: #ccc;
	background-color: white;
	opacity: 0.8;
	align-content: center;
	width: 100%;
	text-align: center;
}

.pettable td {
	font-family: Arial, sans-serif;
	font-size: 16px;
	font-weight: bold;
	padding: 16px 8px;
	overflow: hidden;
	word-break: normal;
	border- color: black;
	color: black;
}

.pettable th {
	font-family: Arial, sans-serif;
	font-size: 16px;
	font-weight: bold;
	padding: 16px 8px;
	border-style: solid;
	border-width: 2px;
	overflow: hidden;
	word-break: normal;
	border- color: #ccc;
	color: tomato;
}
</style>
</head>
<body>
	<div class="header">
		<div class="topnav">
			<a class="active" href="#"><i class="fa fa-fw fa-home"></i>
				PETSHOP</a>

			<div class="topnav-right">
				<a href="login" class="active">Logout</a>
			</div>
			<div class="topnav-right">
				<a href="home"><i class="fa fa-fw fa-search"></i> Home</a> <a
					href="myPets"><i class="fa fa-fw fa-envelope"></i> My PET</a> <a
					href="addPetPage"><i class="fa fa-fw fa-user"></i> Add Pet</a>
			</div>
		</div>
		<table class="pettable" border="1" align="center">
			<tr>
				<th width="100"><b>PET Id</b></th>
				<th width="100"><b>Name</b></th>
				<th width="100"><b>Age</b></th>
				<th width="100"><b>Place</b></th>
				<th width="100"><b>Buy</b></th>
			</tr>
			<c:forEach items="${petList}" var="pet">
				<tr>
					<td>${pet.petId}</td>
					<td>${pet.petName}</td>
					<td>${pet.petAge}</td>
					<td>${pet.petPlace}</td>
					<td><c:choose>
							<c:when test="${empty pet.user.userId}">
								<a href="<c:url value='./buyPet?petId=${pet.petId}' />
">Buy</a>
							</c:when>
							<c:otherwise>
								<p>Sold out</p>
							</c:otherwise>
						</c:choose></td>
				</tr>
			</c:forEach>
		</table>
	</div>
</body>
</html>