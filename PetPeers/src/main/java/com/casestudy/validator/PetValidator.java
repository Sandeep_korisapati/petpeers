package com.casestudy.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.casestudy.model.Pet;

@Component
public class PetValidator implements Validator{

	@Override
	public boolean supports(Class<?> arg0) {
		return Pet.class.equals(arg0);
	}

	@Override
	public void validate(Object arg0, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "petName", "key2");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "petAge", "key2");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "petPlace", "key2");
	}
	
	public void validateAge(Pet pet, Errors errors) {
		if(pet.getPetAge()<0 || pet.getPetAge()>99) {
			errors.rejectValue("petAge", "msg4");
		}
	}

}
