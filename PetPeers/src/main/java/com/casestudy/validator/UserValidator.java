package com.casestudy.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.casestudy.model.User;
import com.casestudy.service.UserService;


@Component
public class UserValidator implements Validator{

	@Autowired
	private UserService userService;
	@Override
	public boolean supports(Class<?> arg0) {
		return false;
	}

	@Override
	public void validate(Object arg0, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "userName", "key1");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "userPassword", "key1");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "confirmPassword", "key1");
	}
	public boolean validatePassword(User user, Errors errors) {
        if (!user.getUserPassword().equals(user.getConfirmPassword())) {
            errors.rejectValue("confirmPassword", "msg1");
            return true;
        }
        else {
        	return false;
        }
    }
	
	public boolean validateUserName(User user, Errors errors) {
		User user1 = userService.saveUser(user);
		if(user1==null) {
			errors.rejectValue("userName", "msg2");
			return true;
		}
		else {
			return false;
		}
	}

}
