package com.casestudy.dao;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.casestudy.model.User;

@Repository
public class UserDAOImpl implements UserDAO {
	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public User saveUser(User user) {
		Session session = this.sessionFactory.getCurrentSession();
		Query query = session.createQuery(" select userName from User where userName=:userName");
		query.setParameter("userName", user.getUserName());
		List<User> usernames =(((org.hibernate.query.Query) query).list());
		if(usernames.size()>0) {
			return new User();
		}
		else {
			session.save(user);
			return user;
		}
		
		// }

	}

	@SuppressWarnings({ "unchecked", "rawtypes", "unlikely-arg-type" })
	@Override
	public User authenticateUser(String username, String password) {

		Session session = this.sessionFactory.getCurrentSession();
		Query query = session.createQuery(" select userPassword from User where userName=:userName");
		query.setParameter("userName", username);
		List<User> userPassword =(((org.hibernate.query.Query) query).list());

		if (userPassword.contains(password)) {

			return new User();

		}
		return null;

	}

}
