package com.casestudy.dao;

import java.util.List;


import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.casestudy.model.Pet;
import com.casestudy.model.User;

@Repository
public class PetDAOImpl implements PetDAO {

	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	private User user;

	@SuppressWarnings({ "unchecked" })
	@Override
	public List<Pet> getAllPets() {
		Session session = this.sessionFactory.getCurrentSession();
		Query query = session.createQuery("from pet", Pet.class);
		List<Pet> listOfPets = query.getResultList();
		return listOfPets;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Pet> getMyPets(int userId) {
		List<Pet> mypets = null;
		Session session = this.sessionFactory.getCurrentSession();
		Query query = session.createQuery("from pet where PETOWNERID=:i");
		query.setParameter("i", userId);
		List<Pet> listOfPets = query.getResultList();
		
		if(listOfPets != null) {
			mypets = listOfPets;
		}
		return mypets;
	}

	@Override
	public Pet savePet(Pet pet) {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(pet);
		return null;
	}

	@Override
	public Pet buyPet(int petId, int userId) {
		// logger().info("inside the buyPet business logic - DAO Impl Layer");
		Session session = this.sessionFactory.openSession();
		Pet pet = session.load(Pet.class,(long)petId);
		if(pet!=null) {
			user.setUserId(userId);
			pet.setUser(user);
			this.sessionFactory.openSession().update(pet);
		}
		return pet;
	}
}
