package com.casestudy.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;


@Component
@Entity(name="pet")
@Table(name="PET")
public class Pet implements Serializable{
/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//	petId (Long), petName (String), petAge (Integer), petPlace (String), user (User)
	@Id
	@GeneratedValue
	private long petId;
	private String petName;
	private int petAge;
	private String petPlace;
	@ManyToOne
	@JoinColumn(name="PETOWNERID")
	private User user;
	public long getPetId() {
		return petId;
	}
	public void setPetId(long petId) {
		this.petId = petId;
	}
	public String getPetName() {
		return petName;
	}
	public void setPetName(String petName) {
		this.petName = petName;
	}
	public int getPetAge() {
		return petAge;
	}
	public void setPetAge(int petAge) {
		this.petAge = petAge;
	}
	public String getPetPlace() {
		return petPlace;
	}
	public void setPetPlace(String petPlace) {
		this.petPlace = petPlace;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
}
