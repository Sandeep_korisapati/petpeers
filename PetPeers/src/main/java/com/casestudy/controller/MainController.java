package com.casestudy.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.casestudy.model.Pet;
import com.casestudy.model.User;
import com.casestudy.service.PetService;
import com.casestudy.service.UserService;
import com.casestudy.validator.LoginValidator;
import com.casestudy.validator.PetValidator;
import com.casestudy.validator.UserValidator;

@Controller
public class MainController {
	private static Logger logger = LogManager.getLogger(MainController.class);
	@Autowired
	private User user;
	@Autowired
	private Pet pet;
	@Autowired
	private UserService userService;
	@Autowired
	private PetService petService;
	@Autowired
	private UserValidator userValidator;
	@Autowired
	private LoginValidator loginValidator;
	@Autowired
	private PetValidator petValidator;

	@RequestMapping(value = "/")
	public ModelAndView index() {
		logger.info("index() page");
		ModelAndView modelAndView = new ModelAndView("registrationPage");
		modelAndView.addObject("user", user);
		return modelAndView;
	}

	@RequestMapping(value = "/login")
	public ModelAndView login() {
		logger.info("login() page");
		ModelAndView modelAndView = new ModelAndView("loginPage");
		modelAndView.addObject("loginModel", user);
		return modelAndView;
	}

	@RequestMapping(value = "/register")
	public ModelAndView register(@ModelAttribute("user") User user) {
		logger.info("register() page");
		ModelAndView modelAndView = new ModelAndView("registrationPage");
		// modelAndView.addObject("registerModel");
		return modelAndView;
	}

	@RequestMapping(value = "/saveUser", method = RequestMethod.POST)
	public ModelAndView saveUser(@ModelAttribute("user") User user, BindingResult result, Model map) {
		logger.info("saveUser() page");
		userValidator.validate(user, result);
		userValidator.validateUserName(user, result);
		userValidator.validatePassword(user, result);
		// userService.saveUser(user);
		ModelAndView modelAndView = null;
		if (result.hasErrors()) {
			modelAndView = new ModelAndView("registrationPage");
		} else {

			User user1 = new User();
			map.addAttribute("loginModel", user1);
			modelAndView = new ModelAndView("loginPage");
		}
		return modelAndView;
	}

	/*
	 * @RequestMapping(value = "/authenticateUser") public ModelAndView
	 * authenticateUser(HttpServletRequest request, @ModelAttribute("loginModel")
	 * User user, BindingResult result) { ModelAndView modelAndView = null;
	 * loginValidator.validate(user, result);
	 * loginValidator.authenticate(user,result); if (result.hasErrors()) {
	 * modelAndView = new ModelAndView("loginPage"); } else { modelAndView = new
	 * ModelAndView("redirect:/home"); } return modelAndView; }
	 */
	@RequestMapping(value = "/authenticateUser")
	public ModelAndView authenticateUser(HttpServletRequest request, @ModelAttribute("loginModel") User user,
			BindingResult result, HttpSession session) {
		logger.info("authenticateUser() page");
		ModelAndView modelAndView = null;
		loginValidator.validate(user, result);
		loginValidator.authenticate(user, result);
		if (result.hasErrors()) {
			modelAndView = new ModelAndView("loginPage");
		} else {
			// if(request.getSession().getAttribute("sessionStatus") != null) {
			User user1 = userService.authenticateUser(user.getUserName(), user.getUserPassword());
			if (user1 != null) {
				// session = request.getSession();
				session.setAttribute("user", user1);
				/*
				 * request.getSession().setAttribute("sessionStatus",true);
				 * request.getSession().setAttribute("userid",user.getUserId()); //
				 * request.getSession().setAttribute("username",user.getUserName()); //
				 * request.getSession().setAttribute("password",user.getPassword());
				 */ modelAndView = new ModelAndView("redirect:/home");
			} else {
				modelAndView = new ModelAndView("redirect:/login");
			}
			
		}
		return modelAndView;
	}

	@RequestMapping(value = "/logout")
	public ModelAndView logout(HttpServletRequest request) {
		logger.info("logout() page");
		request.getSession().invalidate();
		ModelAndView modelAndView = new ModelAndView("redirect:/login");
		return modelAndView;
	}

	@RequestMapping(value = "/home")
	public ModelAndView home(HttpServletRequest request) {
		logger.info("home() page");
		ModelAndView modelAndView = null;
		modelAndView = new ModelAndView("homePage");
		List<Pet> list = petService.getAllPets();
		modelAndView.addObject("petList", list);
		return modelAndView;
	}
	/*
	 * @RequestMapping(value ="/addPetPage") public ModelAndView addPet() {
	 * ModelAndView modelAndView = new ModelAndView("addPetPage");
	 * modelAndView.addObject("pet", pet); return modelAndView; }
	 * 
	 * @RequestMapping(value="/savePet") public ModelAndView
	 * savePet(@ModelAttribute("pet")Pet pet,BindingResult result) {
	 * petValidator.validate(pet, result); petValidator.validateAge(pet, result);
	 * ModelAndView modelAndView = null; if(result.hasErrors()) { modelAndView = new
	 * ModelAndView("addPetPage"); } else{ petService.savePet(pet); modelAndView =
	 * new ModelAndView("homePage"); } return modelAndView; }
	 */

	@RequestMapping(value = "/addPetPage")
	public ModelAndView addPet() {
		logger.info("addpet() page");
		ModelAndView modelAndView = new ModelAndView("addPetPage");
		modelAndView.addObject("pet", pet);
		return modelAndView;
	}

	@RequestMapping(value = "/savePet")
	public ModelAndView savePet(@ModelAttribute("pet") Pet pet, BindingResult result) {
		logger.info("savePet() page");
		petValidator.validate(pet, result);
		petValidator.validateAge(pet, result);
		ModelAndView modelAndView = null;
		if (result.hasErrors()) {
			modelAndView = new ModelAndView("addPetPage");
		} else {
			petService.savePet(pet);
			modelAndView = new ModelAndView("redirect:/home");
		}
		return modelAndView;
	}

	@RequestMapping(value = "/myPets")
	public ModelAndView myPets(HttpServletRequest request) {
		logger.info("mypets() page");
		ModelAndView modelAndView = new ModelAndView("myPetsPage");
		HttpSession httpSession = request.getSession();
		User currentUser = (User) httpSession.getAttribute("user");
		int userId = currentUser.getUserId();
		List<Pet> petList = petService.getMyPets(userId);
		modelAndView = modelAndView.addObject("petList", petList);
		return modelAndView;
	}

	@RequestMapping(value = "/buyPet", method = RequestMethod.GET)
	public ModelAndView buyPet(HttpServletRequest request) {
		logger.info("buyPet() page");
		String stringPetId = request.getParameter("petId");
		int petId = Integer.parseInt(stringPetId);
		ModelAndView modelAndView = null;
		HttpSession httpSession = request.getSession();
		User currentUser = (User) httpSession.getAttribute("user");
		int userId = currentUser.getUserId();
		System.out.println(userId);
		petService.buyPet( petId,userId);
		modelAndView = new ModelAndView("redirect:/myPets");

		return modelAndView;
	}
}
