package com.casestudy.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.casestudy.dao.PetDAO;
import com.casestudy.model.Pet;

@Service
public class PetServiceImpl implements PetService {
	@Autowired
	private PetDAO petDAO;

	@Override
	@Transactional
	public List<Pet> getAllPets() {
		List<Pet> list = petDAO.getAllPets();
		return list;
	}

	@Override
	@Transactional
	public List<Pet> getMyPets(int userId) {
		List<Pet> list = petDAO.getMyPets(userId);
		return list;
	}

	@Override
	@Transactional
	public Pet savePet(Pet pet) {

		return petDAO.savePet(pet);
	}

	@Override
	@Transactional
	public Pet buyPet(int petId, int userId) {
		petDAO.buyPet(petId, userId);
		return null;
	}

}
