package com.casestudy.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.casestudy.dao.UserDAO;
import com.casestudy.model.User;

@Service
public class UserServiceImpl implements UserService {
	@Autowired
	private UserDAO userDAO;
	@Override
	@Transactional
	public User saveUser(User user) {
		User userDoa = null;
		userDoa = userDAO.saveUser(user);
		return userDoa;
	}

	@Override
	@Transactional
	public User authenticateUser(String username, String password) {
		User userDoa = null;
		userDoa = userDAO.authenticateUser(username, password);
		return userDoa;
	}

}
